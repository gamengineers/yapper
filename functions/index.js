const functions = require('firebase-functions');

// The Firebase admin SDK to access the Firebase Realtime Database
// const admin = require('firebase-admin');
// admin.initializeApp(functions.config().firebase);

const app = require('express')();

app.get('/:user/:wager/:bet/', (req, res) => {
  res.send(`bet is ${req.params.user} - ${req.params.wager} - ${req.params.bet}`);
  res.send(req.query.text);
});


// We name this function "route", which you can see is
// still surfaced in the HTTP URLs below.
module.exports = functions.https.onRequest(app);
// cmonbet.me/rt/
// cmonbet.me/r/twilight/2boxes/ridicuolous


// const Firestore = require('@google-cloud/firestore');
// const firestore = new Firestore();


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// Take the text parameter passed to this HTTP endpoint, and
// insert it into the Realtime Database under the path /messages/:pushId/original
// exports.addMessage = functions.https.onRequest((req, res) => {
//   // Grab the test parameter
//   const original = req.query.text;

//   // Push the new message into the Realtime Database using the Firebase Admin SDK
//   admin.database().ref('/bets').push({original: original}).then(snapshot => {
//     // Redirect with 303 SEE Other to the URL of the pushed object in the Firebase console
//     res.redirect(303, snapshot.ref);
//   });
// });