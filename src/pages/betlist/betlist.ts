import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AuthProvider } from '../../providers/auth/auth';

export interface Item {
  id: string;
  bet: string;
  wager: string;
  createdAt: string;
  expiresAt: string; // default 1 week
}
/**
 * Generated class for the BetListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 * https://stackoverflow.com/questions/46900430/firestore-getting-documents-id-from-collection
 */

@IonicPage({
  name: 'my-bets',
  segment: 'listAll'
})
@Component({
  selector: 'page-betlist',
  templateUrl: 'betlist.html',
})
export class BetListPage implements OnInit {

  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;


  constructor(private readonly afs: AngularFirestore, public auth: AuthProvider, public navCtrl: NavController, public navParams: NavParams) {

    console.log(navParams);

    this.itemsCollection = this.afs.collection<Item>('bets');
    // .valueChanges() is simple. It just returns the
    // JSON data without metadata. If you need the
    // doc.id() in the value you must persist it your self
    // or use .snapshotChanges() instead. See the addItem()
    // method below for how to persist the id with
    // valueChanges()
    this.items = this.itemsCollection.valueChanges();
  }

  ngOnInit() {
    console.log('ngOnInit');
  }

  // private handleError(error: Error): Promise<Error> {
  //   console.error('An error occurred', error); // for demo purposes only
  //   return Promise.reject(error.message || error);
  // }

  ionViewDidLoad() {
    // Hide view if NFL api is not available
    console.log('ionViewDidLoad');
  }

  itemSelected(item) {
    // load page with this item
    this.navCtrl.push('bet', {
      matchup: item,
      userProfile: this.navParams.data.userProfile
    });
  }

  addBet(){
    console.log('Added bet');
    this.navCtrl.push('make-bet');
  }

  logout() {
    this.auth.logoutUser().then(res => {
      console.log(res);
      this.navCtrl.push('login');
    });
  }

}