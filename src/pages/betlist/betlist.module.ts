import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BetListPage } from './betlist';

@NgModule({
  declarations: [
    BetListPage,
  ],
  imports: [
    IonicPageModule.forChild(BetListPage),
  ],
})
export class BetListPageModule {}
