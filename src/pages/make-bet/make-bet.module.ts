import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MakeBetPage } from './make-bet';

@NgModule({
  declarations: [
    MakeBetPage,
  ],
  imports: [
    IonicPageModule.forChild(MakeBetPage),
  ],
})
export class MakeBetPageModule {}
