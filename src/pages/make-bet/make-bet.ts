import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase'; // used for timestamp
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
// import { Observable } from 'rxjs/Observable';

export interface Item {
  id: string;
  bet: string;
  wager: string;
  createdAt: any;
}

/**
 * Generated class for the MakeBetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'make-bet',
  segment: 'r/:user/:wager/:bet'
})
@Component({
  selector: 'page-make-bet',
  templateUrl: 'make-bet.html',
})
export class MakeBetPage {

  private betForm: FormGroup;
  private itemsCollection: AngularFirestoreCollection<Item>;

  constructor(
      private readonly afs: AngularFirestore,
      public navCtrl: NavController,
      public navParams: NavParams,
      private formBuilder: FormBuilder)
   {

     console.log(this.navParams);
     this.itemsCollection = afs.collection<Item>('bets');

     // this.user =this.navParams.data.userProfile;

     this.betForm = this.formBuilder.group({
       mybet: ['',Validators.compose([Validators.maxLength(280), Validators.required])],
       mywager: ['', Validators.required],
     });
  }

  submitBet() {
    // let { bet, wager } = this.betForm.controls;
    this.addBet().then(willSend => {
      console.log(willSend);
      // if (willSend) {
      //   this.navCtrl.push('bet', {
      //     bet: bet.value,
      //     wager: wager.value
      //   });
      // }
    })
  }

  /* @todo hash url https://progblog.io/Create-a-URL-Shortener-with-Cloud-Firestore-and-Cloud-Functions/ */
  addBet(): Promise<boolean> {

    let { mybet: { value :  bet }, mywager: { value : wager } } = this.betForm.controls;
    // let { email, photoURL } = this.user;

    return new Promise((resolve, reject) => {
      if (this.betForm.valid) {
        const createdAt = firebase.firestore.FieldValue.serverTimestamp();
        // Persist a document id
        const id = this.afs.createId();
        const item: Item = { id, bet, wager, createdAt };
        this.itemsCollection.add(item);
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

}
