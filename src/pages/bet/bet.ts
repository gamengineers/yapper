import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'bet',
  segment: 'bet/:user/:wager/:bet' // user/robertersaud/bet/34555333322 'deep link'
})
@Component({
  selector: 'page-bet',
  templateUrl: 'bet.html',
})
export class BetPage {

  /*
  *  show bet for user
  *  yapper.com/#/user/ghostrunners/bet/343434334
  *
  */

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log(navParams);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BetPage');
  }

}
