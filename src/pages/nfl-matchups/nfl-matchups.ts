import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers} from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toPromise';
// import { ChangeDetectorRef } from '@angular/core';

// import { Observable } from 'rxjs/Observable';


/**
 * Generated class for the NflMatchupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'nfl-matchups'
})
@Component({
  selector: 'page-nfl-matchups',
  templateUrl: 'nfl-matchups.html',
})
export class NflMatchupsPage implements OnInit {

  public matchups: any;
  public currentSeason: number;
  public currentWeek: number;
  public currentSchedule: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http/*, private cdr: ChangeDetectorRef */) {
  }

  ngOnInit() {
    this.getSchedule();
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive

  }

  createSubscriptionHeader(headers: Headers) {
    headers.append('Ocp-Apim-Subscription-Key', environment.fantasyData.subscriptionKey);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getUrl(url): Promise<any> {
    let headers = new Headers();
    this.createSubscriptionHeader(headers);
    return this.http.get(url, { headers: headers })
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  reduceSchedule(raw, week): Promise<any> {
    let schedule = raw.json();
    return new Promise((resolve, reject) => {
      // map number of users currently in room to each matchup
      resolve(schedule.filter(match => match.AwayTeam !== 'BYE' && match.Week === week) || []);
    });
  }

  getSchedule() {
    let headers = new Headers();
    this.createSubscriptionHeader(headers);

    let season = 'https://api.fantasydata.net/v3/nfl/scores/JSON/CurrentSeason';
    let week = 'https://api.fantasydata.net/v3/nfl/scores/JSON/CurrentWeek';

    Promise.all([this.getUrl(season), this.getUrl(week)])
      .then(res => {
        if (res.length > 0) {
          this.currentSeason = res[0];
          this.currentWeek = res[1];
          this.http.get(`https://api.fantasydata.net/v3/nfl/scores/JSON/Schedules/${this.currentSeason}`, { headers: headers })
            .map(res => this.reduceSchedule(res, this.currentWeek))
            .subscribe(res => this.currentSchedule = res);
         }
      });
  }

  ionViewDidLoad() {
    // Hide view if NFL api is not available
    return this.getUrl('https://api.fantasydata.net/v3/nfl/scores/JSON/Ping')
      .then(res => res !== undefined);
  }

  itemSelected(item) {
    // load page with this item
    this.navCtrl.push('chat', {
      matchup: item,
      userProfile: this.navParams.data.userProfile
    });
  }

}
