import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NflMatchupsPage } from './nfl-matchups';

@NgModule({
  declarations: [
    NflMatchupsPage,
  ],
  imports: [
    IonicPageModule.forChild(NflMatchupsPage),
  ],
})
export class NflMatchupsPageModule {}
