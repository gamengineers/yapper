import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BetsPage } from './bets';

@NgModule({
  declarations: [
    BetsPage,
  ],
  imports: [
    IonicPageModule.forChild(BetsPage),
  ],
})
export class BetsPageModule {}
