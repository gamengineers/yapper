import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BetsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'bets',
  segment: '/bets/:user'
})
@Component({
  selector: 'page-bets',
  templateUrl: 'bets.html',
})
export class BetsPage {


  /*
   *  List bets per logged in user
   *  If none,  show most recent public bets
  */

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BetsPage');
  }

}
