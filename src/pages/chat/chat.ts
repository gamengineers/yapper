import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase';

interface Item {
  user: string;
  createdAt: string;
  updatedAt: string;
  gameKey: string;
  message: string;
  avatar: any;
};

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'chat',
  defaultHistory: ['nfl-matchups']
})
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  public items$: Observable<Item[]>;
  public user: any;
  public gameKey: string;
  public myMessage: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFirestore) {
    this.user = this.navParams.data.userProfile;

    // @todo use ionic storage to store current room
    this.gameKey = navParams.data && navParams.data.matchup && navParams.data.matchup.GameKey;

    // @todo filter by gameKey first, then short in snapshot
    let itemRef = db.collection<Item>('chats', ref => ref.orderBy('createdAt'));
    this.items$ = itemRef.snapshotChanges().map(actions => {
        return actions.map(a => a.payload.doc.data() as Item).filter(i => i.gameKey === this.gameKey);
     });
  }

  addDocument(event) {
    if (event !== undefined) {
        const timestamp = firebase.firestore.FieldValue.serverTimestamp();
        this.db.collection('chats').add({
          user: this.user && this.user.displayName,
          avatar: this.user && this.user.photoURL,
          createdAt: timestamp,
          updatedAt: timestamp,
          message: event,
          gameKey: this.gameKey
       });
       this.myMessage = '';
    }
  }

}
