import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase/app';

export interface Item {
  id: string;
  bet: string;
  wager: string;
  createdAt: string;
  expiresAt: string; // default 1 week
}

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(private readonly afs: AngularFirestore, public navCtrl: NavController, public navParams: NavParams, public auth: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  googleLogin() {
    this.auth.googleLogin().then(user => {
      let profile = user && user.additionalUserInfo && user.additionalUserInfo.profile;
      // let db = this.afs.collection('users');
      // let query = db.where('userProfile.email', '==', profile.email).then(function(doc) {
      //   if (doc.exists) {
      //    console.log("Document data:", doc.data());
      //     } else {
      //         console.log("No such document!");
      //     }
      // }).catch(function(error) {
      //     console.log("Error getting document:", error);
      // });
      this.userExists(profile).subscribe(result => {
        console.debug('userExists: ' + user + ' | ' + result);
        if (result.length === 0) {
          this.navCtrl.push('register', profile );
        } else {
          this.navCtrl.push('my-bets', { user: profile });
        }
      });

     // db.collection("cities").get().then(function(querySnapshot) {
     //   querySnapshot.forEach(function(doc) {
     //     console.log(doc.id, " => ", doc.data());
     //   });
     // });

    });
  }

  userExists(profile): Observable<any> {
    console.log('LOGIN#PRofile: ' + profile);
    return this.afs.collection<any>('users', ref => {
      let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      query = query.where('profile.email', '==', profile.email)
      return query;
    }).valueChanges();
  }

  facebookLogin() {
    console.debug('Not yet implemented');
  }

  logout() {
    this.auth.logoutUser();
  }

}
