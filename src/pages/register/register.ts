import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/switchMap';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import firebase from 'firebase/app';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'register'
})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  private itemsCollection: AngularFirestoreCollection<any>;
  private userprofile: any;

  user$: BehaviorSubject<string|null>;
  queryObservable$: Observable<any[]>;

  private userForm: FormGroup;


  constructor(private afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder) {


    this.itemsCollection = afs.collection<any>('users');

    this.userprofile = navParams.data;

    this.userForm = this.formBuilder.group({
       username: ['', Validators.compose([Validators.maxLength(20), Validators.required])],
    });

    this.user$ = new BehaviorSubject(null);
    this.queryObservable$ = this.user$.switchMap(user =>
      afs.collection('users', ref => {
        let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
        if (user) {
          query = query.where('username', '==', user)
        }
        return query;
      }).valueChanges()
    );

    // subscribe to changes
    // @todo cross reference against email to prevent duplicate username
    this.queryObservable$.subscribe(queriedItems => {
      if (queriedItems.length === 0) {
        this.addItem(this.userForm.controls.username.value, this.userprofile);
      } else {
        console.log("ALert, username already exists");
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  submit() {
    // trigger the query
    this.user$.next(this.userForm.controls.username.value);
  }

  addItem(username: string, profile: any) {
    // Persist a document id
    const id = this.afs.createId();
    // const userProfile = profile.userProfile;
    const item: any = { id, username, profile };
    Promise.all([this.itemsCollection.add(item)]).then(res => {
      this.navCtrl.push('my-bets', { user: item });
    });
  }

}
