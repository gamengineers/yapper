export const environment = {
	production: false,
	tmdb: {
		v3: 'ca54187e8cb438628c8b2e511b97109c',
		v4: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjYTU0MTg3ZThjYjQzODYyOGM4YjJlNTExYjk3MTA5YyIsInN1YiI6IjU2ZWQ3MzYzYzNhMzY4MjI2ZTAwNzMwZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.__08vRA4BctVIEXxfj9Ac-_Sw6q9_SHRWdzPri52tdc'
	},
	fantasyData: {
		subscriptionKey: '6058e6813cb54b30bb0c6b96b0c509a9'
	},
  firebase: {
    apiKey: "AIzaSyAnEWHCbC_0lN4TAcK6-Tn3lrflBLVEtf4",
    authDomain: "smackchat-d3bca.firebaseapp.com",
    databaseURL: "https://smackchat-d3bca.firebaseio.com",
    projectId: "smackchat-d3bca",
    storageBucket: "smackchat-d3bca.appspot.com",
    messagingSenderId: "398947032784"
  }
};